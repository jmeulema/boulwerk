package be.meulemans.boulwerk;

import be.meulemans.boulwerk.generic.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AncestralGameTest {

    @Test
    public void testFullGame() {
        Game game = new AncestralGame();
        game.roll(8);
        game.roll(1);
        game.roll(1);

        game.roll(8);
        game.roll(7);

        game.roll(1);
        game.roll(2);
        game.roll(1);

        game.roll(15);

        game.roll(1);
        game.roll(2);
        game.roll(1);
        Assertions.assertEquals(55, game.score());
    }

    @Test
    public void testFullGameWithExtraRoll() {
        Game game = new AncestralGame();
        game.roll(15);

        game.roll(8);
        game.roll(1);
        game.roll(2);

        game.roll(1);
        game.roll(2);
        game.roll(12);

        game.roll(6);
        game.roll(4);
        game.roll(1);

        game.roll(15);
        game.roll(8);
        game.roll(2);
        game.roll(3);
        Assertions.assertEquals(101, game.score());
    }
}
package be.meulemans.boulwerk.generic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class FrameTest {

    @Test
    public void testRollNoPin() {
        Frame frame = new Frame(5, 3, 3, 2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertEquals(0, frame.fallenPins());
        frame.roll(0);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertEquals(0, frame.fallenPins());
    }

    @Test
    public void testRollOnePin() {
        Frame frame = new Frame(5, 3, 3, 2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertEquals(0, frame.fallenPins());
        frame.roll(1);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(4, frame.remainingPins());
        Assertions.assertEquals(1, frame.fallenPins());
    }

    @Test
    public void testRollManyPins() {
        Frame frame = new Frame(5, 3, 3, 2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertEquals(0, frame.fallenPins());

        frame.roll(3);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(2, frame.remainingPins());
        Assertions.assertEquals(3, frame.fallenPins());

        frame.roll(1);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(1, frame.remainingPins());
        Assertions.assertEquals(4, frame.fallenPins());

        frame.roll(1);
        Assertions.assertTrue(frame.isComplete());
        Assertions.assertEquals(0, frame.remainingPins());
        Assertions.assertEquals(5, frame.fallenPins());
    }

    @Test
    public void testRollStrike() {
        Frame frame = new Frame(5, 3, 3, 2);
        frame.roll(5);
        Assertions.assertTrue(frame.isComplete());
        Assertions.assertEquals(0, frame.remainingPins());
        Assertions.assertEquals(5, frame.fallenPins());
    }

    @Test
    public void testRollSpare() {
        Frame frame = new Frame(5, 3, 3, 2);
        frame.roll(1);
        frame.roll(4);
        Assertions.assertTrue(frame.isComplete());
        Assertions.assertEquals(0, frame.remainingPins());
        Assertions.assertEquals(5, frame.fallenPins());
    }

    @Test
    public void testIsSpare() {
        Frame frame = new Frame(5, 3, 3, 2);
        frame.roll(1);
        Assertions.assertFalse(frame.isSpare());
        frame.roll(4);
        Assertions.assertTrue(frame.isSpare());
    }

    @Test
    public void testIsStrike() {
        Frame frame = new Frame(5, 3, 3, 2);
        frame.roll(5);
        Assertions.assertTrue(frame.isStrike());
        frame = new Frame(5, 3, 3, 2);
        frame.roll(4);
        Assertions.assertFalse(frame.isStrike());
    }
}
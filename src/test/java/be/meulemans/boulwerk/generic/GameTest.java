package be.meulemans.boulwerk.generic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class GameTest {

    @Test
    public void testFirstThrow() {
        Game game = new Game(5, 15, 3, 3, 2);
        game.roll(1);
        Assertions.assertEquals(1, game.score());
    }

    @Test
    public void testFullGameWithOnlyOnes() {
        Game game = new Game(5, 15, 3, 3, 2);
        for (int i = 0; i < 15; i++) {
            game.roll(1);
        }
        Assertions.assertEquals(15, game.score());
    }

    @Test
    public void testRollInvalidNumberOfPins() {
        Game game = new Game(5, 5, 3, 3, 2);

        Assertions.assertThrows(IllegalArgumentException.class, () -> game.roll(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> game.roll(7));

        game.roll(1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> game.roll(5));
    }

    @Test
    public void testGameWithStrike() {
        Game game = new Game(5, 15, 3, 3, 2);
        game.roll(15);

        game.roll(1);
        game.roll(2);
        game.roll(1);
        Assertions.assertEquals(23, game.score());
    }

    @Test
    public void testGameWithTwoConsecutiveStrikes() {
        Game game = new Game(5, 15, 3, 3, 2);
        game.roll(15);
        game.roll(15);

        game.roll(1);
        game.roll(1);
        game.roll(1);
        Assertions.assertEquals(53, game.score());
    }

    @Test
    public void testGameWithSpare() {
        Game game = new Game(5, 15, 3, 3, 2);
        game.roll(8);
        game.roll(7);

        game.roll(1);
        game.roll(2);
        game.roll(1);
        Assertions.assertEquals(22, game.score());
    }

    @Test
    public void testGameWithTwoConsecutiveSpare() {
        Game game = new Game(5, 15, 3, 3, 2);
        game.roll(1);
        game.roll(14);
        game.roll(1);
        game.roll(14);
        game.roll(1);
        game.roll(1);
        game.roll(1);
        Assertions.assertEquals(50, game.score());
    }

    @Test
    public void testGameWithOnlyStrike() {
        Game game = new Game(5, 15, 3, 3, 2);
        game.roll(15);
        game.roll(15);
        game.roll(15);
        game.roll(15);
        game.roll(15);
        game.roll(15);
        game.roll(15);
        game.roll(15);
        Assertions.assertTrue(game.isComplete());
        Assertions.assertEquals(300, game.score());
    }

    @Test
    public void testFullGame() {
        Game game = new Game(6, 8, 2, 4, 2);
        game.roll(7);
        game.roll(1);
        game.roll(1);
        game.roll(7);
        game.roll(7);
        game.roll(1);
        game.roll(2);
        game.roll(1);
        game.roll(8);
        game.roll(1);
        game.roll(2);
        Assertions.assertEquals(60, game.score(), game.scoreBoard());
    }

    @Test
    public void testIncompleteGame() {
        Game game = new Game(5, 5, 2, 2, 1);
        game.roll(5);
        game.roll(3);
        Assertions.assertFalse(game.isComplete());
        Assertions.assertEquals(11, game.score());
        Assertions.assertFalse(game.scoreBoard().isEmpty());
    }
}
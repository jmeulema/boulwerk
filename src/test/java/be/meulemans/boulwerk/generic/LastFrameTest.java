package be.meulemans.boulwerk.generic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

public class LastFrameTest {

    @Test
    public void testNoSpareNorStrike() {
        LastFrame frame = new LastFrame(5, 3, 3, 2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());

        frame.roll(1);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(4, frame.remainingPins());

        frame.roll(2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(2, frame.remainingPins());

        frame.roll(1);
        Assertions.assertTrue(frame.isComplete());
        Assertions.assertEquals(1, frame.remainingPins());
        Assertions.assertEquals(4, frame.fallenPins());
        Assertions.assertFalse(frame.isSpare());
        Assertions.assertFalse(frame.isStrike());
    }

    @Test
    public void testStrike() {
        LastFrame frame = new LastFrame(5, 3, 3, 2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertFalse(frame.isStrike());
        Assertions.assertFalse(frame.isSpare());

        frame.roll(5);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertTrue(frame.isStrike());
        Assertions.assertFalse(frame.isSpare());

        frame.roll(1);
        frame.roll(1);
        frame.roll(1);
        Assertions.assertTrue(frame.isComplete());
        Assertions.assertEquals(2, frame.remainingPins());
        Assertions.assertEquals(8, frame.fallenPins());
        Assertions.assertTrue(frame.isStrike());
        Assertions.assertFalse(frame.isSpare());
        Assertions.assertEquals(8, frame.score(Stream.empty()));
    }

    @Test
    public void testSpare() {
        LastFrame frame = new LastFrame(5, 3, 3, 2);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertFalse(frame.isStrike());
        Assertions.assertFalse(frame.isSpare());

        frame.roll(4);
        frame.roll(1);
        Assertions.assertFalse(frame.isComplete());
        Assertions.assertEquals(5, frame.remainingPins());
        Assertions.assertFalse(frame.isStrike());
        Assertions.assertTrue(frame.isSpare());

        frame.roll(1);
        frame.roll(1);
        frame.roll(1);
        Assertions.assertTrue(frame.isComplete());
        Assertions.assertEquals(2, frame.remainingPins());
        Assertions.assertEquals(8, frame.fallenPins());
        Assertions.assertFalse(frame.isStrike());
        Assertions.assertTrue(frame.isSpare());
    }
}
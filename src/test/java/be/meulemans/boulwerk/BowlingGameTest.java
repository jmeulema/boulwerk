package be.meulemans.boulwerk;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BowlingGameTest {

    @Test
    public void testFullGame() {
        BowlingGame game = new BowlingGame();

        game.roll(5);
        game.roll(3);

        game.roll(10);

        game.roll(5);
        game.roll(5);

        game.roll(2);
        game.roll(0);

        game.roll(4);
        game.roll(2);

        game.roll(10);

        game.roll(2);
        game.roll(5);

        game.roll(2);
        game.roll(8);

        game.roll(10);

        game.roll(3);
        game.roll(4);

        Assertions.assertEquals(116, game.score());
    }

    @Test
    public void testFullGameWithExtraRoll() {
        BowlingGame game = new BowlingGame();

        game.roll(5);
        game.roll(3);

        game.roll(10);

        game.roll(5);
        game.roll(5);

        game.roll(2);
        game.roll(0);

        game.roll(4);
        game.roll(2);

        game.roll(10);

        game.roll(2);
        game.roll(5);

        game.roll(2);
        game.roll(8);

        game.roll(10);

        game.roll(10);
        game.roll(7);
        game.roll(2);

        Assertions.assertTrue(game.isComplete());
        Assertions.assertEquals(138, game.score());
    }
}
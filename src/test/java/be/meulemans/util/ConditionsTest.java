package be.meulemans.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConditionsTest {

    @Test
    public void testCheckArgument() {
        Conditions.checkArgument(true, "error");
        Assertions.assertThrows(IllegalArgumentException.class, () -> Conditions.checkArgument(false, "error"));
    }

    @Test
    public void testCheckArgumentInRange() {
        Conditions.checkArgumentInRange(1, 0, 2, "test");
        Conditions.checkArgumentInRange(1, 1, 2, "test");
        Conditions.checkArgumentInRange(1, 0, 1, "test");
        Assertions.assertThrows(IllegalArgumentException.class, () -> Conditions.checkArgumentInRange(3, 0, 1, "test"));
    }

    @Test
    public void testCheckState() {
        Conditions.checkState(true, "error");
        Assertions.assertThrows(IllegalStateException.class, () -> Conditions.checkState(false, "error"));
    }

    @Test
    public void testCheckStateValueInRange() {
        Conditions.checkStateValueInRange(1, 0, 2, "test");
        Conditions.checkStateValueInRange(1, 1, 2, "test");
        Conditions.checkStateValueInRange(1, 0, 1, "test");
        Assertions.assertThrows(IllegalStateException.class, () -> Conditions.checkStateValueInRange(3, 0, 1, "test"));
    }

    @Test
    public void testCheckNotNull() {
        Conditions.checkNotNull(new Object());
        Assertions.assertThrows(NullPointerException.class, () -> Conditions.checkNotNull(null));
        Conditions.checkNotNull(new Object(), "error");
        Assertions.assertThrows(NullPointerException.class, () -> Conditions.checkNotNull(null, "error"));
    }

}
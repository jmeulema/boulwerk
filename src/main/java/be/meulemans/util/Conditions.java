package be.meulemans.util;

/**
 * Static convenience methods that help to check conditions at runtime.
 * If a condition is not met, an unchecked exception of a specified type is thrown.
 * <p>
 * This class is partially inspired by Preconditions.java from the Guava Library:
 * https://google.github.io/guava/releases/snapshot/api/docs/com/google/common/base/Preconditions.html
 */
public final class Conditions {

    /**
     * This cannot be instantiated
     */
    private Conditions() {
        throw new UnsupportedOperationException();
    }

    /**
     * Ensures the truth of an expression involving one or more parameters to the calling method.
     *
     * @param expression           a boolean expression to test
     * @param errorMessageTemplate a template for the exception message should the check fail. (not null)
     * @param errorMessageArgs     the arguments to be substituted into the message template.
     * @throws IllegalArgumentException if {@code expression} is false
     */
    public static void checkArgument(boolean expression, String errorMessageTemplate, Object... errorMessageArgs) {
        checkNotNull(errorMessageTemplate, "error message template");
        if (!expression) {
            throw new IllegalArgumentException(String.format(errorMessageTemplate, errorMessageArgs));
        }
    }

    /**
     * Ensures that {@code argument} is in the range from {@code lowest}, inclusive, to {@code highest}, inclusive.
     *
     * @param argument     an argument to test
     * @param lowest       the lowest possible value for the argument
     * @param highest      the highest possible value for the argument
     * @param argumentName the argument name that will be used to create the error message if needed
     * @throws IllegalArgumentException if {@code argument} is not the range
     */
    public static void checkArgumentInRange(int argument, int lowest, int highest, String argumentName) {
        checkNotNull(argumentName, "argument name");
        if (argument < lowest || argument > highest) {
            throw new IllegalArgumentException(String.format("%s was %d but should be in range [%d,%d]", argumentName, argument, lowest, highest));
        }
    }

    /**
     * Ensures the truth of an expression involving the state of the calling instance, but not involving any parameters to the calling method.
     *
     * @param expression           a boolean expression to test
     * @param errorMessageTemplate a template for the exception message should the check fail. (not null)
     * @param errorMessageArgs     the arguments to be substituted into the message template.
     * @throws IllegalStateException if {@code expression} is false
     */
    public static void checkState(boolean expression, String errorMessageTemplate, Object... errorMessageArgs) {
        checkNotNull(errorMessageTemplate, "error message template");
        if (!expression) {
            throw new IllegalStateException(String.format(errorMessageTemplate, errorMessageArgs));
        }
    }

    /**
     * Ensures the {@code value} involving the state of the calling instance, is a number in the range from {@code lowest}, inclusive, to {@code
     * highest}, inclusive.
     *
     * @param value     a value to test
     * @param lowest    the lowest possible value
     * @param highest   the highest possible value
     * @param valueName the value name that will be used to create the error message if needed
     * @throws IllegalStateException if {@code value} is not the range
     */
    public static void checkStateValueInRange(int value, int lowest, int highest, String valueName) {
        checkNotNull(valueName, "value name");
        if (value < lowest || value > highest) {
            throw new IllegalStateException(String.format("%s was %d but should be in range [%d,%d]", valueName, value, lowest, highest));
        }
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference an object reference
     * @throws NullPointerException if {@code reference} is null
     */
    public static void checkNotNull(Object reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference     an object reference
     * @param referenceName the reference name that will be used to create the error message if needed
     * @throws NullPointerException if {@code reference} is null
     */
    public static void checkNotNull(Object reference, String referenceName) {
        if (reference == null) {
            throw new NullPointerException(String.format("%s was null but should not be", referenceName));
        }
    }

}

package be.meulemans;


import be.meulemans.boulwerk.AncestralGame;

public class Main {

    public static void main(String[] args) {
        AncestralGame game = new AncestralGame();

        game.roll(15);

        game.roll(8);
        game.roll(1);
        game.roll(2);

        game.roll(1);
        game.roll(2);
        game.roll(12);

        game.roll(6);
        game.roll(4);
        game.roll(1);

        game.roll(15);
        game.roll(8);
        game.roll(2);
        game.roll(3);

        System.out.println(game.scoreBoard());
    }
}

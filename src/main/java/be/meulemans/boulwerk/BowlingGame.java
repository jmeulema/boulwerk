package be.meulemans.boulwerk;

import be.meulemans.boulwerk.generic.Game;

/**
 * This class helps to track the score of one player for the game 'Bowling'.
 */
public final class BowlingGame extends Game {

    public BowlingGame() {
        super(10, 10, 2, 2, 1);
    }
}

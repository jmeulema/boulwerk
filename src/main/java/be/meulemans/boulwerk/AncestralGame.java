package be.meulemans.boulwerk;

import be.meulemans.boulwerk.generic.Game;

/**
 * This class helps to track the score of one player for the ancestral game 'Boulwerk' played by the Africans in the old ages.
 * This game is similar to current bowling expect that:
 * - 15 pins are used in the game
 * - maximum 3 tries are allowed to knock down all the pins
 * - 5 sequences of games are played by each player try to knock down the pins
 * - a strike bonus takes the 3 following rolls into account
 * - a spare bonus takes the 2 following rolls into account
 */
public final class AncestralGame extends Game {

    public AncestralGame() {
        super(5, 15, 3, 3, 2);
    }

}

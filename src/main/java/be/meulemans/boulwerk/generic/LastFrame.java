package be.meulemans.boulwerk.generic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static be.meulemans.util.Conditions.*;

/**
 * The last sub-part of a game where a player tries to knock down all the pins with a maximum of try.
 * This frame also allows the user to play the bonus in case of strike or spare.
 */
final class LastFrame extends Frame {
    private final List<Integer> extraFallenPins = new ArrayList<>();

    LastFrame(int existingPins, int maxRolls, int strikeBonusScope, int spareBonusScope) {
        super(existingPins, maxRolls, strikeBonusScope, spareBonusScope);
        checkClassInvariant();
    }

    /*
     * Class Invariant:
     *   extraFallenPins != null
     *   extraFallenPins.size <= max(strikeBonusScope, spareBonusScope)
     *   for all p in extraFallenPins, p != null and p in [0, existingPins]
     */
    private void checkClassInvariant() {
        // Should be deactivated in production mode (depends on the way we would determine this)
        checkNotNull(extraFallenPins);
        checkState(
                extraFallenPins.size() <= Math.max(strikeBonusScope, spareBonusScope),
                "too many extra rolls have been stored: %d / %d",
                extraFallenPins.size(), Math.max(strikeBonusScope, spareBonusScope));
        for (Integer extraFallenPin : extraFallenPins) {
            checkNotNull(extraFallenPin);
            checkStateValueInRange(extraFallenPin, 0, existingPins, "extra fallen pins in one roll");
        }
    }

    @Override
    protected void saveRoll(int pins) {
        if (isStrike() || isSpare()) {
            extraFallenPins.add(pins);
        } else {
            super.saveRoll(pins);
        }
        checkClassInvariant();
    }

    @Override
    boolean isComplete() {
        if (isStrike()) {
            return extraFallenPins.size() == strikeBonusScope;
        }
        if (isSpare()) {
            return extraFallenPins.size() == strikeBonusScope;
        }
        return super.isComplete();
    }

    @Override
    int rolls() {
        return super.rolls() + extraFallenPins.size();
    }

    @Override
    int remainingPins() {
        return existingPins - (fallenPins() % existingPins);
    }

    @Override
    int fallenPins() {
        return super.fallenPins() + fallenExtraPins();
    }

    private int fallenExtraPins() {
        return extraFallenPins.stream().reduce(0, Integer::sum);
    }

    @Override
    boolean isSpare() {
        return super.rolls() > 1 && super.fallenPins() == existingPins;
    }

    @Override
    int score(Stream<Integer> nextRolls) {
        return fallenPins();
    }

    @Override
    Stream<Integer> fallenPinsStream() {
        return Stream.concat(super.fallenPinsStream(), extraFallenPins.stream());
    }
}

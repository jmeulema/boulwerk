package be.meulemans.boulwerk.generic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static be.meulemans.util.Conditions.*;

/**
 * The sub-part of a game where a player tries to knock down all the pins with a maximum of try.
 */
class Frame {
    protected final int existingPins;
    private final int maxRolls;
    protected final int strikeBonusScope;
    protected final int spareBonusScope;
    private final List<Integer> fallenPins = new ArrayList<>();

    Frame(int existingPins, int maxRolls, int strikeBonusScope, int spareBonusScope) {
        checkArgument(existingPins > 1, "'existing pins' was %d but should be at least 1", existingPins);
        checkArgument(maxRolls > 1, "'maximum rolls' was %d but should be at least 1", maxRolls);
        checkArgument(strikeBonusScope > 0, "'strike bonus scope' was %d but should be positive", strikeBonusScope);
        checkArgument(spareBonusScope > 0, "'spare bonus scope' was %d but should be positive", spareBonusScope);

        this.existingPins = existingPins;
        this.maxRolls = maxRolls;
        this.strikeBonusScope = strikeBonusScope;
        this.spareBonusScope = spareBonusScope;
        checkClassInvariant();
    }

    /*
     * Class Invariant:
     *   fallenPins != null
     *   fallenPins.size <= maxRolls
     *   for all p in fallenPins, p != null and p in [0, existingPins]
     *   sum of fallenPins elements in [0, existingPins]
     */
    private void checkClassInvariant() {
        // Should be deactivated in production mode (depends on the way we would determine this)

        checkNotNull(fallenPins);
        checkState(
                fallenPins.size() <= maxRolls,
                "too many rolls have been stored: %d / %d",
                fallenPins.size(), maxRolls);
        for (Integer fallenPin : fallenPins) {
            checkNotNull(fallenPin);
            checkStateValueInRange(fallenPin, 0, existingPins, "fallen pins in one roll");
        }
        checkStateValueInRange(
                fallenPins.stream().reduce(0, Integer::sum),
                0,
                existingPins,
                "total fallen pins");
    }

    void roll(int fallenPins) {
        checkState(!isComplete(), "frame is already complete");
        checkArgumentInRange(fallenPins, 0, remainingPins(), "fallen pins");

        saveRoll(fallenPins);
        checkClassInvariant();
    }

    protected void saveRoll(int pins) {
        fallenPins.add(pins);
    }

    int remainingPins() {
        return existingPins - fallenPins();
    }

    boolean isComplete() {
        return fallenPins.size() == maxRolls || remainingPins() == 0;
    }

    int rolls() {
        return fallenPins.size();
    }

    int fallenPins() {
        return fallenPins.stream().reduce(0, Integer::sum);
    }

    Stream<Integer> fallenPinsStream() {
        return fallenPins.stream();
    }

    boolean isStrike() {
        return !fallenPins.isEmpty() && fallenPins.get(0) == existingPins;
    }

    boolean isSpare() {
        return fallenPins.size() > 1 && fallenPins() == existingPins;
    }

    int score(Stream<Integer> nextRolls) {
        if (isSpare()) {
            return fallenPins() + nextRolls.limit(spareBonusScope).reduce(0, Integer::sum);
        }
        if (isStrike()) {
            return fallenPins() + nextRolls.limit(strikeBonusScope).reduce(0, Integer::sum);
        }
        return fallenPins();
    }
}

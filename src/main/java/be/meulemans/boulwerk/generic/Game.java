package be.meulemans.boulwerk.generic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static be.meulemans.util.Conditions.*;

/**
 * This class helps to track the score of one player for a generic game in which a number of pins are laid out and needs to be knocked down by the
 * roll of a ball.
 */
public class Game {
    private final int numberOfFrames;
    private final int strikeBonusScope;
    private final int spareBonusScope;
    private final int existingPins;
    private final int numberOfRollsPerFrame;

    private final List<Frame> frames = new ArrayList<>();

    /**
     * The creation of a generic game with explicit definition of the game parameters:
     *
     * @param numberOfFrames        : the number of successive try to knock down all the pins (at least 1)
     * @param existingPins          : the number of existing pins to knock down at once (at least 1)
     * @param numberOfRollsPerFrame : the maximum number of try to knock down the existing pins (at least 1)
     * @param strikeBonusScope      : the number of following rolls that will be used to compute the bonus after a strike
     * @param spareBonusScope       : the number of following rolls that will be used to compute the bonus after a spare
     */
    public Game(int numberOfFrames, int existingPins, int numberOfRollsPerFrame, int strikeBonusScope, int spareBonusScope) {
        checkArgument(numberOfFrames > 1, "'number of frames' was %d but should be at least 1", numberOfFrames);
        checkArgument(existingPins > 1, "'existing pins' was %d but should be at least 1", existingPins);
        checkArgument(numberOfRollsPerFrame > 1, "'number of rolls per frame' was %d but should be at least 1", numberOfRollsPerFrame);
        checkArgument(strikeBonusScope > 0, "'strike bonus scope' was %d but should be positive", strikeBonusScope);
        checkArgument(spareBonusScope > 0, "'spare bonus scope' was %d but should be positive", spareBonusScope);

        this.numberOfFrames = numberOfFrames;
        this.existingPins = existingPins;
        this.numberOfRollsPerFrame = numberOfRollsPerFrame;
        this.strikeBonusScope = strikeBonusScope;
        this.spareBonusScope = spareBonusScope;
        addFrame();

        checkClassInvariant();
    }

    /*
     * Class Invariant:
     *   frames.size() in [1, numberOfFrames]
     *   for all i | i in [0, frames.size() -2]: frames(i) is complete
     */
    private void checkClassInvariant() {
        // Should be deactivated in production mode (depends on the way we would determine this)

        checkStateValueInRange(frames.size(), 1, numberOfFrames, "number of frames");
        for (int i = 0; i < frames.size() - 2; i++) {
            checkState(frames.get(i).isComplete(), "all non last frame should be complete");
        }
    }

    /**
     * Notify the game a given number of pins have fallen after the roll of a ball.
     *
     * @param fallenPins : the number of pins that have fallen (cannot exceed the number of remaining pins)
     */
    public void roll(int fallenPins) {
        checkState(!isComplete(), "game is already complete");

        Frame frame = frames.get(frames.size() - 1);
        checkArgumentInRange(fallenPins, 0, frame.remainingPins(), "fallen pins");

        frame.roll(fallenPins);

        if (frame.isComplete() && !this.isComplete()) {
            addFrame();
        }

        checkClassInvariant();
    }

    private void addFrame() {
        if (frames.size() == numberOfFrames) {
            throw new IllegalStateException("All the frames have already been created");
        }
        Frame frame;
        if (frames.size() == numberOfFrames - 1) {
            frame = new LastFrame(existingPins, numberOfRollsPerFrame, strikeBonusScope, spareBonusScope);
        } else {
            frame = new Frame(existingPins, numberOfRollsPerFrame, strikeBonusScope, spareBonusScope);
        }
        frames.add(frame);
    }

    /**
     * Compute the current score of the game.
     * Bonuses are computed as if zero pins felt if the required roll has not yet been played.
     *
     * @return : the current score
     */
    public int score() {
        int score = 0;
        for (int i = 0; i < frames.size(); i++) {
            score += score(i);
        }
        return score;
    }

    private int score(int frameIndex) {
        Stream<Integer> nextRolls = frames.stream().skip(frameIndex + 1L).flatMap(Frame::fallenPinsStream);
        return frames.get(frameIndex).score(nextRolls);
    }

    /**
     * @return a textual representation of the different rolls and the evolution of the score
     */
    public String scoreBoard() {
        StringBuilder board = new StringBuilder();
        int score = 0;
        for (int i = 0; i < numberOfFrames; i++) {
            board.append("* ").append(i + 1).append(" * \t");
            if (i < frames.size()) {
                String frameRolls = frames.get(i).fallenPinsStream()
                        .map(Object::toString)
                        .collect(Collectors.joining(", "));
                board.append(frameRolls).append(System.lineSeparator());
                score += score(i);
                board.append(" ---> ").append(score);
            } else {
                board.append(System.lineSeparator()).append(System.lineSeparator());
            }
            board.append(System.lineSeparator());
        }
        return board.toString();
    }

    /**
     * @return true if the game if finished and will not accept any roll
     */
    public boolean isComplete() {
        return frames.size() == numberOfFrames && frames.get(frames.size() - 1).isComplete();
    }
}
